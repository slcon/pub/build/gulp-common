<!--****************************************************************************
* $Id: README.md 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
*****************************************************************************-->
# @slcpub/gulp-common

Provides a base framework of common functions and predefined tasks for use in 
[`gulpfile.js`][Gulp] files that build packages using [npm] for version management. 
This is also a set of tools and infrastructure for creating other build packages, 
like this one, to serve more specific build environments.

<!-- toc -->

- [Installation](#installation)
- [Usage in `gulpfile.js`](#usage-in-gulpfilejs)
  * [Available Tasks](#available-tasks)
    + [push](#push)
    + [undo-version](#undo-version)
    + [update-readme-toc](#update-readme-toc)
  * [Task Usage Help](#task-usage-help)
    + [Showing Help Content](#showing-help-content)
    + [Including Package Tasks](#including-package-tasks)
- [Usage in Creating a Build Package](#usage-in-creating-a-build-package)
  * [Package Structure](#package-structure)
  * [Function Modules](#function-modules)
    + [Inline Functions](#inline-functions)
  * [Task Modules](#task-modules)
    + [Inline Task Functions](#inline-task-functions)
    + [Task Dependencies](#task-dependencies)
  * [Usage](#usage)
- [API](#api)
  * [Options](#options)
  * [Properties](#properties)
  * [Functions](#functions)
    + [addToPath](#addtopath)
    + [addTasks](#addtasks)
    + [fatal](#fatal)
    + [getPackageInfo](#getpackageinfo)
    + [makePathProperty](#makepathproperty)
    + [normalizeEOL](#normalizeeol)
    + [removeFromPath](#removefrompath)
    + [run](#run)
    + [runTasks](#runtasks)
    + [showUsage](#showusage)
    + [updateTOC](#updatetoc)
    + [useGulp](#usegulp)
  * [Package Building Functions](#package-building-functions)
    + [makeModule](#makemodule)
    + [addTasksFromMap](#addtasksfrommap)
    + [getTaskMap](#gettaskmap)
    + [makeAddTasks](#makeaddtasks)
    + [makeFunctions](#makefunctions)
- [Building This Package](#building-this-package)
  * [Build Commands](#build-commands)
  * [TL;DR](#tldr)
  * [`gulp-npm` Dependency](#gulp-npm-dependency)
- [See Also](#see-also)

<!-- tocstop -->

## Installation

  1) Set the endpoint for the [package registry] using [npm].
  1) Install this package as a development dependency:

```bash
npm install --save-dev @slcpub/gulp-common
```
[package registry]: https://gitlab.com/slcon/pub/registry#npm

## Usage in `gulpfile.js`

To use this package in the project build process, add it to `gulpfile.js` and 
load any desired tasks:

```js
const gulp   = require('gulp');
const common = require('@slcpub/gulp-common').useGulp(gulp);
common({options});
common.addTasks(task...);
```
  - `common({options})` sets any desired [options].
  - `common.addTasks(task...)` adds some or all of the [available tasks] to 
    `gulp` using `gulp.task()`.

### Available Tasks

These tasks may be added to the Gulp instance by calling [addTasks].

#### push

runs `git push && git push --tags` to push local repo changes, including tags
to the remote repo. Should be run before the new version of the library has been 
published.

#### undo-version

Reverts the local Git repository changes from the last [npm version] command.

This task will:
  1) Read `package.json` for the current version tag,
  1) Verify the version tag exists in the repo and points to the HEAD tag,
  1) Verify there are no local changes,
  1) Reset the repo to the previous commit,
  1) Remove the version tag, and
  1) Remove local changes from the previous commit.

[npm version]: https://docs.npmjs.com/cli/v6/commands/npm-version

#### update-readme-toc

Replaces <code>&lt;!-- toc --&gt;</code> tag in `README.md` with generated table of contents
using [updateTOC](#updatetoc).

### Task Usage Help

[gulp-help-doc] is included to provide usage help for Gulp tasks from comments found preceeding 
a task definition:

```js
/**
* This is My Cool Task and it does some really cool stuff!
* There is much more to My Cool Task than meets the eye!
*
* @task  {my-cool-task}
* @group {Main Tasks}
* @order {10}
*/
gulp.task('my-cool-task', [ 'my-dependency' ],
  () => console.log('This is a cool task!'));
```
 
#### Showing Help Content

The [showUsage] function will print the usage help for all tasks with usage content defined.
Here is an example of printing help content, including task usage, with the `default` task:

```js
gulp.task('default', () => 
  {
  console.log(`
    This is my build file for My Cool Project. It is truly awesome and
    it includes My Cool Task!
    `.replace(/^\s+/gm, ''));
  return common.showUsage();
  });
```
Running `gulp [default]` will produce the help content on the console:

```
This is my build file for My Cool Project. It is truly awesome and
it includes My Cool Task!

Usage: gulp [task] [options]
Tasks:
 Main Tasks
    my-cool-task              This is My Cool Task and it does some really cool stuff!
                              There is much more to My Cool Task than meets the eye!
                              Depends: ["my-dependency"]
```

#### Including Package Tasks

To include help content in the output of [showUsage] from [Tasks][Available Tasks] [added to Gulp][addTasks], 
this package must be added to the `helpDocDependencies` list in `package.json`:

```json
{
  ...
  "helpDocDependencies": [
    "@slcpub/gulp-common"
  ],
  ...
}
```

The help content for the added tasks are assigned to these `@order/@group` definitions:

  - 100 - Build Tasks
  - 200 - Supporting Tasks

[showUsage]:     #showusage
[gulp-help-doc]: https://github.com/softlife/gulp-help-doc

## Usage in Creating a Build Package

This package provides a basic infrastructure for creating other build packages 
that provide common tasks and functions for more specific build environments.

As of this writing, other build packages for the NuGet and npm environments have 
been built using this package. These packages provide functions and tasks for
use in `gulpfile.js` more specific to those environments.

### Package Structure

By default, a build package folder structure looks like this:

```
package/
|-- funcs/
|   |-- Function Modules
|-- tasks/
|   |-- Task Modules
|   index.js
|   package.json
|   README.md
```

### Function Modules

[CommonJS] modules in this folder are injected with a reference to the package module and 
return a function to be added to the package with the base name of the module file.

For example, the function returned from the module in `funcs/myCoolFunction.js` is added 
to the package as follows:

```js
$package['myCoolFunction'] = require('funcs/myCoolFunction.js')($package)
```

#### Inline Functions

Functions can also be easily added to the package module in `index.js`:

```js
$package.myCoolFunction = (value) => console.log(`Here is my cool value: ${value}`);
```

### Task Modules

[CommonJS] modules in this folder are called, not injected, and return a function to be 
added to the [Gulp Instance] with [addTasks]. The absolute file path of these modules are 
added to the module [task map] with the base name of the module file.

For example, the file path of `tasks/myCoolTask.js` is added to the task map as follows:

```js
$package.tasks['myCoolTask'] = '<PackagePath>/tasks/myCoolTask.js';
```

#### Inline Task Functions

Task functions may also be added directly to the task map in `index.js`:

```js
$package.tasks.myCoolTask = () => console.log('Executing myCoolTask!');
```

#### Task Dependencies

Tasks with dependencies may provide them by task name in the task function 
`.dependencies` array property. For example, in a [CommonJS] Task Module:

```js
module.exports              = () => console.log('Executing myCoolTask!');
module.exports.dependencies = ['myCoolDependency'];
```

And, in an Inline Task Function:
```js
$package.tasks.myCoolTask              = () => console.log('Executing myCoolTask!');
$package.tasks.myCoolTask.dependencies = ['myCoolDependency'];
```

[commonjs]: https://nodejs.org/docs/latest/api/modules.html


### Usage

To use this package to create another build package, add it to `index.js` or
whatever the [main package file] is:

```javascript
const common  = require('@slcpub/gulp-common');
const $self   = module.exports = common.makeModule(__dirname);
$self.options = {options}; 
```

[makeModule] does the whole job of creating the package module function and there 
should never be a reason to do so manually. But, if it becomes necessary, here is 
what it does:

```js
const common   = require('@slcpub/gulp-common');
const $module  = common.makeModule.defaultFunction;
const $self    = Object.assign($module, common.makeFunctions($module, __dirname)
module.exports = Object.assign($self,
  {
  addTasks: common.makeAddTasks(),
  tasks:    common.getTaskMap(__dirname),
  useGulp:  common.makeModule.useGulp    
  });
```

[main package file]: https://docs.npmjs.com/cli/v7/configuring-npm/package-json#main

## API

### Options

Module options may be set with the package module function or by setting the
options directly:

```js
const common = require('@slcpub/gulp-common');
common({options});
common.options = {options};
```

  - **logger** `{object}`  
    Logging utility (console, by default). May be changed to [fancy-log] or 
    some other utility that provides a **`log()`** function.

[fancy-log]: https://github.com/gulpjs/fancy-log

### Properties

These properties live on the package module function returned from `require()`:

  - <a name="gulp-instance"></a>**gulp** `{object}`  
    Gulp instance. Set using [useGulp].

  - <a name="module-options"></a>**options** `{object}`  
    Module options.

  - <a name="task-map"></a>**tasks** `{object}`  
    Map of task names to task module file path, or task function, for tasks 
    that may be added to `gulp` using [addtasks].

    Set by [makeModule] from a call to [getTaskMap].

### Functions

#### addToPath

Adds path segments to the PATH environment variable.

  - ***parameters***
    - `{string[]|string...}` Path segment(s) to add.

#### addTasks 

Adds [available tasks] to the [Gulp instance].

  - ***parameters***
    - `{string[]|string...}`
      Name of the task to add.  
      All tasks are added if no names are supplied.

#### fatal

Returns `Error` object.

  - ***parameters***
    - `{string}` Error message.
  - ***returns***
    - `{Error}`  Error object with `.message` set to input.

#### getPackageInfo

Returns `package.json` content. Throws fatal if `name` or `version` is missing.

  - ***returns***  
    - `{object}` `package.json` content.

#### makePathProperty

Returns a Javascript property descriptor, for use with `Object.defineProperties()`, 
creating accessors for a path segment property that will add the segment to the PATH
environment variable when a new value is assigned. This is useful for creating a module 
options property of a path segment that must be added to PATH to make an external tool 
available. For example,

```js
$self.options = Object.defineProperties({
  property1: 'value1',
  property2: 'value2',
},
  common.makePathProperty('OtherToolPath')
);

$self.options.OtherToolPath = 'C:\MyTools' // Adds 'C:\MyTools' to PATH.
```

  - ***parameters***
    - `{string}` Property name.
  - ***returns***  
    - `{string}` Property descriptors for use with `Object.defineProperties()`.

#### normalizeEOL

Converts line endings `(\r?\n)` to EOL character(s) for the environment.

  - ***parameters***
    - `{string}` String to normalize.
  - ***returns***
    - `{Error}`  Input with normalized line endings for the environment.

#### removeFromPath

Removes path segments to the PATH environment variable.

  - ***parameters***
    - `{string[]|string...}` Path segment(s) to remove.

#### run

Runs an external command. The command and command output is printed to the console as 
"stdout[\nstderr]" on success. If the command fails, the returned promise is rejected 
with fatal "err.message" or `{error:err.message, stdout:stdout, stderr:stderr}` 
depending on `nonfatal` option.

  - ***parameters***
    - `{string}` External command.
    - `{options}`  
      - **quiet**    `{boolean}`  
        `true`=do not echo successful command output.
      - **silent**   `{boolean}`  
        `true`=do not echo command name or successful output.
      - **nonfatal** `{boolean}`  
        `true`=reject errors with error information.  
        `false`=reject errors with fatal error message.   
      - Remaining options will be passed to [`child_process.exec()`].
  - ***returns***  
    - `{promise}` Resolved with command output, rejected with fatal error info.

[`child_process.exec()`]: https://nodejs.org/api/child_process.html#child_processexeccommand-options-callback

#### runTasks

Runs a list of tasks synchronously.

  - ***parameters***
    - `{string...}` Task name(s).
  - ***returns***  
    - `{promise}` Resolved on success, rejected with fatal error info.

#### showUsage

Print a list of the tasks installed to `gulp` using [gulp-help-doc].

  - ***usage***
    ```js
    const gulp   = require('gulp');
    const common = require('@slcpub/gulp-common').useGulp(gulp);
    gulp.task('default', () => common.showUsage()); 
    ```

[gulp-help-doc]: https://github.com/softlife/gulp-help-doc

#### updateTOC

Finds markdown files matching the input [glob specification][globs] and inserts or updates a 
Table of Contents using `markdown-toc`. Returns a promise resolved when all files found have 
been updated. When a file is updated, the returned promise is notified with a [Vinyl file object][vinyl].

Returns a file stream that inserts a Table of Contents to markdown file(s) using [markdown-toc].

  - ***parameters***
    - `{string}|{string[]}` File glob specification.
    - `{object}` `markdown-toc` options.
  - ***returns***  
    - `{promise}` Resolved when complete and notified when a file is modified.
  - ***usage***
    ```js
    gulp.task('update-markdown-files', () => 
      common.updateTOC('*/**/*.md', {options})
        .progress(file => console.log(`Updated: ${file.relative}`)); 
    ```

[markdown-toc]: https://github.com/jonschlinkert/markdown-toc
[globs]:        https://gulpjs.com/docs/en/getting-started/explaining-globs/
[vinyl]:        https://github.com/gulpjs/vinyl

#### useGulp

Assigns the Gulp instance to use when adding and running tasks.

  - ***parameters***
    - `{object}` Gulp instance.
  - ***returns***  
    - `{function}` Self-reference.
  - ***usage***
    ```js
    const gulp   = require('gulp');
    const common = require('@slcpub/gulp-common').useGulp(gulp);
    ```

### Package Building Functions

#### makeModule

Returns the package module function to assign to `module.exports`. By default,
this function takes input of an options object and updates the module [options] 
property.

This function will:
  1) create the module function from the default or the given alternate function,
  1) call [makeFunctions](#makefunctions) to add [function modules],
  1) call [getTaskMap] to load the [task map],
  1) call [makeAddTasks](#makeaddtasks) to add the [addTasks] function, and 
  1) create the [useGulp] function to assign the [Gulp instance].

  - ***parameters***
    - `{string}` [`__dirname`] of the package module file.
    - `{function}` Alternate module function.
    - `{string}` Function module file path relative to `__dirname`, default=`funcs`.
    - `{string}` Task module file path relative to `__dirname`, default=`tasks`.
  - ***returns***
    - `{function}` Package module function.

#### addTasksFromMap

Adds a list of tasks from the given task map to the [Gulp instance]. 
[Task modules] are loaded using `require(<filepath>)`.

  - ***parameters***
    - `{object}`
      Map of task names to task module file path or task function.
    - `{string[]|string...}` 
      Name of the task to add. All tasks are added if no names are supplied.

Called by [addTasks] to add tasks from the module [task map].

#### getTaskMap

Reads task module file names and returns map of task names to file paths for
use with [addTasksFromMap]. 

  - ***parameters***
    - `{string}` [`__dirname`] of the package module file.
    - `{string}` Task module file path relative to `__dirname`, default=`tasks`.
  - ***returns***    
    - `{object}` Map of task names to task function file path.

Called by [makeModule] to create the module [task map].

#### makeAddTasks

Returns the module [addTasks] function which calls [addTasksFromMap] to add tasks to the
[Gulp instance] from the module [task map].

  - ***returns***  
    - `{function}` [addTasks] function.

Called by [makeModule] to create the module [addTasks] function.

#### makeFunctions

Injects [function modules] using `require(<filepath>)($module)`, where `$module` 
is the package module, and returns a map of module file base names to the returned functions.

  - ***parameters***
    - `{function}` Package module ($module).
    - `{string}` [`__dirname`] of the package module file.
    - `{string}` Function module file path relative to `__dirname`, default=`funcs`.
  - ***returns***
    - `{object}` Map of function names to functions.

Called by [makeModule] to load [function modules].

## Building This Package

### Build Commands

**`gulp set-registry`** takes input of a [personal access token] or [deploy token] and configures npm for 
[publishing to the package registry][publishing].

[**`npm version`**][npm version] updates **`package.json#version`**, runs **`gulp build`**, commits to
the local repo, and creates a tag for the version.

**`gulp undo-version`** may be run immediately after [npm version]. It reverts the changes and restores
the repository to it's prior state.

[**`npm publish`**][npm publish] is run after **`npm version`** to publish the package to npm registry as defined in 
**`package.json#publishConfig`**.

### TL;DR

After testing is complete and the package is ready to publish:

1) Run [**`npm version`**][npm version]
   * *preversion*: Runs **`npm test`** to run the library tests,
   * Updates the version number in `package.json`,
   * Commits changes and creates a version tag.

1) If, on second thought, the package is not ready,
   * Run **`gulp undo-version`** to remove the tag and restore the repository.

1) If ready to publish,
   * Run **`gulp push`** to update the remote repo with the new version.

1) Run **`gulp set-registry`** to configure npm for publishing to the package registry. This will require
   a [personal access token] or [deploy token].
   
1) Run [**`npm publish`**][npm publish] to publish the package to npm.

[npm publish]:           https://docs.npmjs.com/cli/v8/commands/npm-publish
[npm version]:           https://docs.npmjs.com/cli/v6/commands/npm-version
[publishing]:            https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package
[personal access token]: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens
[deploy token]:          https://docs.gitlab.com/ee/user/project/deploy_tokens/index.html


### `gulp-npm` Dependency

This package is inter-dependent with [`gulp-npm`][gulp-npm] meaning 
they each have the other as a package dependency.

When this package is modified:

  1) Commit, version, and publish `gulp-common`.  
     **Do not push to origin**.
  1) Update `gulp-npm` to use the new `gulp-common`.
  1) Commit, version, push, and publish `gulp-npm`.
  1) Run `gulp undo-version` for `gulp-common`.
  6) Update `gulp-common` to use the new `gulp-npm`.
  7) Remove the previously published `gulp-common`.
  1) Commit, version, push, and publish `gulp-common`.


## See Also

  - [Gulp]
  - [npm]
  - [npm `version` Command][npm version]
  - [How to use `npm version` to create prerelease tags][npm-prerelease]


[addtasks]:         #addtasks
[addTasksFromMap]:  #addtasksfrommap
[available tasks]:  #available-tasks
[function modules]: #function-modules
[getTaskMap]:       #gettaskmap
[gulp instance]:    #gulp-instance
[makemodule]:       #makemodule 
[options]:          #options
[task map]:         #task-map
[task modules]:     #task-modules
[useGulp]:          #usegulp
[gulp]:             https://gulpjs.com/docs/en/getting-started/javascript-and-gulpfiles
[gulp-npm]:         https://gitlab.com/slcon/pub/build/gulp-npm#gulp-npm
[npm]:              https://docs.npmjs.com/about-npm
[npm-prerelease]:   https://jasonraimondi.com/posts/use-the-npm-version-command-to-semantically-version-your-node-project/
[`__dirname`]:      https://nodejs.org/api/modules.html#__dirname
