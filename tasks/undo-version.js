/*****************************************************************************************
* $Id: tasks/undo-version.js b869bd0 2022-04-05 08:55:03 -0700 sthames42 $
/**
* Undo `npm version` command. Must be run immediately following. Resets repo to previous
* commit and removes version tag. Version tag from `package.json` must point to the HEAD
* commit and working tree must not have any modifications.
*
* Version tag may include a prefix (tag-version-prefix) so the tags in the repo are 
* searched by regex for version from `package.json` at the end of the tag.
*
* @task  {undo-version}
* @group {Supporting Tasks}
* @order {200}
*****************************************************************************************/
(function() { 'use strict';

const Q      = require('q');
const common = require('../index');

module.exports = () =>
  {
  const pkgInfo = common.getPackageInfo();
  const version = pkgInfo.version;
  const pattern = '^.*'+version.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')+'$'
  const regex   = new RegExp(pattern, 'm');
  const opts    = { quiet: true };

  return Q.Promise((resolve, reject) =>
    {
    Q.all([common.run('git tag -l', opts), common.run('git status -s -uno', opts)])
      .then(r =>
        {
        var tag = r[0].match(regex);

        if (!tag)
          reject(common.fatal(`no tag found for version: ${version}`));
        
        else if (r[1] != '')
          reject(common.fatal(`working tree is not clean\n${r[1]}`));
        
        else
          {
          Q.all([common.run(`git rev-list -n 1 ${tag}`, opts), common.run('git rev-list -n 1 HEAD', opts)])
            .then(r => 
              {
              if (r[0] != r[1])
                reject(common.fatal(`tag '${tag}' does not point to HEAD`));
              else
                common.run(`git reset ${tag}~1 && git tag -d ${tag} && git restore .`, opts)
                  .then(resolve);
              })
            .catch(reject);
          }
        })
      .catch(reject);
    });
  };
})();
