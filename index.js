/*******************************************************************************
* $Id: index.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
/**
* @name gulp-common
*******************************************************************************/
(function() { 'use strict';

const colors = require('ansi-colors');
const fs     = require('fs');
const fsQ    = fs.promises;
const log    = require('fancy-log');
const os     = require('os');
const path   = require('path');
const Q      = require('q');
const usage  = require('@slcpub/gulp-help-doc');

/*--------------------------------------*/
/* Manually create the module function. */
/*--------------------------------------*/
const makeModule    = require('./funcs/makeModule');
const common        = makeModule.defaultFunction;
const makeFunctions = require('./funcs/makeFunctions')(common);
const $self         = Object.assign(common, makeFunctions(common, __dirname));
module.exports      = Object.assign($self,
  {
  addTasks: common.makeAddTasks(),
  tasks:    common.getTaskMap(__dirname),
  useGulp:  (gulp) => { $self.gulp = gulp; return $self; }
  });

/*--------------------------------------------*/
/* Add `gulp` instance property that throws   */
/* an error if `useGulp` has not been called. */
/*--------------------------------------------*/
Object.defineProperties($self,
  {
  _gulp: { enumerable: false, writable: true },
  gulp:  
    { 
    set: function(gulp) { this._gulp = gulp; },  
    get: function()
      {
      if (!this._gulp || !this._gulp.task)
        throw "gulp instance has not been set. Use `useGulp(gulp)` to set gulp instance.";
      return this._gulp;
      }
    }
  });

/*---------*/
/* Options */
/*---------*/
$self.options = 
  {
  gulp:   undefined,
  logger: { log: log }
  };

/*-----------*/
/* Functions */
/*-----------*/
Object.assign($self, 
  {
  /*---------------------------------------------*/
  /* Temporary until we can upgrade Node and use */
  /* `fs.promises.rmdir(dir, {recursive:true})`. */
  /*---------------------------------------------*/
  rmdir: (fp) => (fs.existsSync(fp) ? $self.run(`rmdir /s/q "${fp.replace(/\//g, path.sep)}"`) : Q.resolve()),
  rm:    (fp) => (fs.existsSync(fp) ? fsQ.unlink(fp) : Q.resolve()),
  mkdir: (fp) => (fs.existsSync(fp) ? Q.resolve()    : fsQ.mkdir(fp)),

  /*****************************************************************************
  * addToPath */
  /**
  * Adds path segments to PATH environment variable.
  * 
  * @param  {[path...]}  Path segment(s) to add.
  *****************************************************************************/
  addToPath: function() 
    {
    var segments = (Array.isArray(arguments[0]) ? arguments[0] : Array.prototype.slice.call(arguments, 0)).filter(s => !!s);
    segments.forEach(p => process.env["PATH"] += `${path.delimiter}${p.replace('/', path.sep)}`);
    },
  
  /*****************************************************************************
  * fatal */
  /**
  * Returns fatal error object for promise rejection to abort the process.
  * 
  * Gulp checks for the presence of a `showStack` property on `Error` objects 
  * passed to a task callback or in the payload of a rejected promise. If this
  * value is not an `Error` object, Gulp throws a format error. This may be 
  * changed in the latest version of Gulp.
  * https://stackoverflow.com/a/39093327/2245849
  *
  * @param  msg  Error message.
  * @returns     Fatal error object that won't show the stack trace.
  *****************************************************************************/
  fatal: (msg) => Object.assign(new Error(colors.red(msg)), {showStack:false}),

  /*****************************************************************************
  * normalizeEOL */
  /**
  * Converts line endings (\r?\n) to EOL character(s) for the environment.
  *
  * @param  s  String to normalize.
  * @return    `s` with line endings normalized.
  *****************************************************************************/
  normalizeEOL: (s) => s.replace(/\r?\n/g, os.EOL),

  /*****************************************************************************
  * removeFromPath */
  /**
  * Removes a path segments previously added with `addToPath`.
  * 
  * @param  {string|string[]}  Path segment(s) to remove.
  *****************************************************************************/
  removeFromPath: function() 
    {
    var segments = (Array.isArray(arguments[0]) ? arguments[0] : Array.prototype.slice.call(arguments, 0)).filter(s => !!s);
    segments.forEach(p => process.env['PATH'] = process.env["PATH"].replace(`${path.delimiter}${p.replace('/', path.sep)}`, ''))
    },

  /*****************************************************************************
  * showUsage */
  /**
  * Returns a call to the `gulp-help-doc` `usage` function to print task
  * documentation.
  *
  * @param  {object}  options  `gulp-help-doc` options.
  * @return {Promise}          Resolved with success.
  *****************************************************************************/
  showUsage: (options) => usage($self.gulp, Object.assign({ lineWidth:100, keysColumnWidth:30 }, options))
  });

/*-------*/
/* Tasks */
/*-------*/
Object.assign($self.tasks,
  {
  /**
  * runs `git push && git push --tags` to push local repo changes, including tags
  * to the remote repo. Should be run after the new version of the library has been 
  * published.
  *
  * @task  {push}
  * @group {Build Tasks}
  * @order {100}
  */
  'push': () => common.run('git push && git push --tags'),

  /**
  * Replaces `<!-- toc -->` tag in `README.md` with a new table of contents.
  *
  * @task  {update-readme-toc}
  * @group {Supporting Tasks}
  * @order {200}
  */
  'update-readme-toc': () => common.updateTOC('./README.md')
  });
})();
