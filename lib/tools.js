/*****************************************************************************************
* $Id$
/**
* Simple utilities
* 
* @name tools
*****************************************************************************************/
module.exports =
  {
  /**
  * `true` when node is running on windows.
  */
  isWindows: (process.platform === 'win32'),

  /**
  * In-memory temporary logger for use in capturing logging output. Color control
  * characters from `ansi-colors` are stripped from logged lines.
  * All lines logged are cleared of display control characters. 
  */
  CleanLogger: function()
    { 
    var _logged = [];
    Object.defineProperties(this,
      {
      logged: { get: () => _logged },
      log:      { value: (msg) => _logged.push(msg.replace(/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g, '')) },
      topLine:  { get:   ()    => _logged.shift() },
      allLines: { get:   ()    => _logged.join('\n') }
      });
    },
  };

