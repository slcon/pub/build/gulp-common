/*******************************************************************************
* $Id$
*******************************************************************************/
const gulp   = require('gulp');
const common = require('./index').useGulp(gulp);
const npm    = require('@slcpub/gulp-npm').useGulp(gulp);

common.addTasks();
npm   .addTasks('set-registry');

gulp.task('default', () => common.showUsage());


