/*******************************************************************************
* $Id: tests/tasks/undo-version.js b869bd0 2022-04-05 08:55:03 -0700 sthames42 $
*******************************************************************************/
const common      = require('../../index');
const expect      = require('chai').expect;
const tools       = require('../lib/test-tools');
const Q           = require('q');
const undoVersion = require('../../tasks/undo-version');

describe('task: undo-version', () => 
  {
  var version = "1.0.0-newstore.2";
  var tags    = 'v1.0.0-newstore.0\nv1.0.0-newstore.1\n1.0.0-newstore.2\n1.0.0-newstore.3';

  before(() => common.getPackageInfo = tools.mock  (common.getPackageInfo, () => ({ "name":"myproject", "version":version })));
  after (() => common.getPackageInfo = tools.unmock(common.getPackageInfo));

  var mockRun = (tags, status, tcommit, hcommit) => 
    common.run = tools.mock(common.run, (cmd) => 
      {
      if (cmd.startsWith('git tag'))    return Q.resolve(tags);
      if (cmd.startsWith('git status')) return Q.resolve(status);
      if (cmd.includes  ('rev-list'))
        return (cmd.includes('HEAD') ? Q.resolve(hcommit) : Q.resolve(tcommit));
      if (cmd.startsWith('git reset'))
        return Q.resolve();
      return Q.reject(`final command '${cmd}' was unexpected.`);
      });

  afterEach(() => common.run = tools.unmock(common.run));

  it('Rejects for no tag found.', (done) =>
    {
    common.run = mockRun('');
    undoVersion()
      .then (() => done(new Error('Promise should have been rejected.')))
      .catch(e =>
        {
        expect(e).to.be.instanceof(Error);
        expect(e.message).to.match(/no tag found/);
        done();
        })
      .catch(done);
    });

  it('Rejects for dirty working tree.', (done) =>
    {
    common.run = mockRun(tags, 'changes');
    undoVersion()
      .then (() => done(new Error('Promise should have been rejected.')))
      .catch(e =>
        {
        expect(e).to.be.instanceof(Error);
        expect(e.message).to.match(/working tree is not clean/);
        done();
        })
      .catch(done);
    });

  it('Rejects for tag/HEAD mismatch.', (done) =>
    {
    common.run = mockRun(tags, '', 'tag', 'head');
    undoVersion()
      .then (() => done(new Error('Promise should have been rejected.')))
      .catch(e =>
        {
        expect(e).to.be.instanceof(Error);
        expect(e.message).to.match(/does not point to HEAD/);
        done();
        })
      .catch(done);
    });

  it('Succeeds.', (done) =>
    {
    common.run = mockRun(tags, '', 'tag', 'tag');
    undoVersion()
      .then(done)
      .catch(e => done(new Error(e)));
    });
  });
