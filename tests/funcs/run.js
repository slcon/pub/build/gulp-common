/*******************************************************************************
* $Id$
*******************************************************************************/
const common    = require('../../index');
const expect    = require('chai').expect;
const tools     = require('../../lib/tools');

describe('run()', () => 
  {
  const goodout = 'this is it';
  const goodcmd = `echo ${goodout}`;
  const badcmd  = 'echo stdout && echo stderr 1>&2 && exit 1';
  var   logger;
  
  before(() => logger = common.options.logger = new tools.CleanLogger());

  it(`Succeeds and logs all output.`, (done) =>
    {
    common.run(goodcmd)
      .then(output =>
        {
        expect(logger.topLine).to.equal(goodcmd);
        expect(logger.topLine).to.equal(goodout);
        expect(output)        .to.equal(goodout);
        done();
        }, done)
      .catch(done)
    })

  it(`Succeeds and logs command but not output (quiet == true).`, (done) => 
    common.run(goodcmd, { quiet:true })
      .then(output =>
        {
        expect(logger.topLine).to.equal(goodcmd);
        expect(logger.topLine).to.be.undefined;
        expect(output).to.equal(goodout);
        done();
        }, done)
      .catch(done)
    )

  it(`Succeeds but does not show any output (silent == true).`, (done) => 
    common.run(goodcmd, { silent:true })
      .then(output =>
        {
        expect(logger.topLine).to.be.undefined;
        expect(output).to.equal(goodout);
        done();
        }, done)
      .catch(done)
    )

  it('Passes options to `child_process.exec`.', (done) =>
    common.run(`echo ${tools.isWindows?"%myval%":"$myval"}`, { silent:true, env: { myval:'testval' }})
      .then(output =>
        {
        expect(output).to.equal('testval');
        done();
        }, done)
      .catch(done)
    )

  it(`Logs command and fails.`, (done) => 
    common.run(badcmd)
      .then(() => done("Command did not fail."))
      .catch(output =>
        {
        expect(output).to.be.an.instanceof(Error);
        done();
        })
      .catch(done)
    )

  it(`Fails with error information (nonfatal == true).`, (done) => 
    common.run(badcmd, { nonfatal:true })
      .then(() => done("Command did not fail."))
      .catch(output =>
        {
        expect(output).to.not.be.an.instanceof(Error);
        expect(output).to.have.property('error',  `Command failed: ${badcmd}\nstderr`);
        expect(output).to.have.property('stderr', 'stderr');
        expect(output).to.have.property('stdout', 'stdout');
        done();
        })
      .catch(done)
    )
  });
