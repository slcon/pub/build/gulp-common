/*******************************************************************************
* $Id: tests/funcs/runTasks.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
*
* We are only testing that `runTasks` resolves/rejects the returned promise when
* the callback function it added to the list of tasks is invoked after
* `run-sequence` calls it.
*******************************************************************************/
const gulp       = require('gulp');
const common     = require('../../index').useGulp(gulp);
const proxyquire = require('proxyquire').noCallThru();

describe('runTasks()', () => 
  {
  /*-------------------------------------------------------------------------*/
  /* `runTasks` uses `run-sequence.use(gulp)` which returns a `run-sequence` */
  /* function bound to `gulp` but we are not using gulp for these tests.     */
  /*                                                                         */
  /* Mock `run-sequence.use` in `runTasks` to return a function that,        */
  /* instead, only invokes the callback to simulate completion of all tasks. */
  /*-------------------------------------------------------------------------*/
  var runTasks = (msg) => proxyquire('../../funcs/runTasks', { 
    'run-sequence':{
      use: () => function() { arguments[arguments.length-1].apply(null, [msg]); }
    }
  })(common);

  it('Resolves a promise when all tasks finish.', (done) =>
    {    
    runTasks()('first', 'second')
      .then (done)
      .catch(e => done(new Error(`Promise was rejected: ${e}.`)));
    })

  it('Rejects a promise when a task fails.', (done) =>
    {    
    runTasks("We failed")('first', 'second')
      .then (() => done(new Error("Promise was resolved.")))
      .catch(() => done());
    })
  });
