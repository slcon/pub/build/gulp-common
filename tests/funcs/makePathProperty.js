/*******************************************************************************
* $Id$
*******************************************************************************/
const common = require('../../index');
const path   = require('path');
const expect = require('chai').expect;

describe('makePathProperty()', () => 
  {
  it('removes old and adds new PATH segment when property is changed.', (done) =>
    {
    const path1 = 'path1/path3';
    const path2 = 'path2/path4'
    const orig  = process.env['PATH'];

    const o = Object.defineProperties({},
      common.makePathProperty('binDir')
      );

    o.binDir = path1;
    expect(process.env['PATH']).to.have.string(path1.replace('/', path.sep));

    o.binDir = path2;
    expect(process.env['PATH']).to.not.have.string(path1.replace('/', path.sep));
    expect(process.env['PATH']).to    .have.string(path2.replace('/', path.sep));

    o.binDir = null;
    expect(process.env['PATH']).to.equal(orig);
    done();
    });
  });
