/*******************************************************************************
* $Id: tests/funcs/addTasksFromMap.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
*******************************************************************************/
const gulp   = require('gulp');
const common = require('../../index').useGulp(gulp);
const tools  = require('../lib/test-tools');
const expect = require('chai').expect;
const path   = require('path');

describe('addTasksFromMap()', () => 
  {
  var tasks = {};
  before(() => gulp.task = tools.mock(gulp.task, (name, deps, func) => tasks[name] = func));
  after(()  => { gulp.task = tools.unmock(gulp.task); tasks = {}});

  it('fails from parameter errors.', (done) =>
    {
    expect(() => common.addTasksFromMap(null)).to.throw(/usage/);
    expect(() => common.addTasksFromMap({}, ['inarray'], 'notarray')).to.throw(/usage/);
    expect(() => common.addTasksFromMap({}, 'unknown')).to.throw(/task 'unknown' is not defined/);
    expect(() => common.addTasksFromMap({ notvalid:50 }, 'notvalid')).to.throw(/task 'notvalid' is not a file path or function./);
    done();
    })

  it('successfully adds tasks.', (done) =>
    {
    var map = common.getTaskMap(path.resolve('./'));
    expect(Object.keys(map).length).to.be.above(0);

    common.addTasksFromMap(map);
    var match  = !Object.keys(map).some(n => !tasks[n]);
    expect(match).to.be.true;

    done();
    })
  });
