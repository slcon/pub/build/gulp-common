/*******************************************************************************
* $Id$
*******************************************************************************/
const common = require('../../index');
const fs     = require('fs');
const path   = require('path');
const expect = require('chai').expect;

describe('getTaskMap()', () => 
  {
  it('loads list of tasks from tasks path.', (done) =>
    {
    var map   = common.getTaskMap(path.resolve('./'));
    expect(Object.keys(map).length > 0).to.be.true;
    var match = !Object.keys(map).some(n => !fs.existsSync(map[n]+'.js'));
    expect(match).to.be.true;
    done();
    })

  it('return empty map for missing tasks path.', (done) =>
    {
    var map = common.getTaskMap(path.resolve('./', 'notasks'));
    expect(Object.keys(map).length === 0).to.be.true;
    done();
    })
  });
