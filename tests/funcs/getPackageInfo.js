/*******************************************************************************
* $Id$
*******************************************************************************/
const common    = require('../../index');
const expect    = require('chai').expect;
const tools     = require('../lib/test-tools');
const fs        = require('fs');

describe('getPackageInfo()', () => 
  {
  before(() => fs.readFileSync = tools.mock  (fs.readFileSync, () => '{"name":"myname","version":"myversion"}'));
  after (() => fs.readFileSync = tools.unmock(fs.readFileSync));

  it('Reads `package.json`.', (done) =>
    {
    var info = common.getPackageInfo();
    expect(info).to.have.property('name',    'myname');
    expect(info).to.have.property('version', 'myversion');
    done();
    });
  });
