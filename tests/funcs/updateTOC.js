/*******************************************************************************
* $Id: tests/funcs/runTasks.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
*
* Mocks `gulp.src` and `gulp.dest` to pass test files to 
*******************************************************************************/
const gulp   = require('gulp');
const common = require('../../index').useGulp(gulp);
const assert = require('chai').assert;
const stream = require('stream');
const Vinyl  = require('vinyl');

describe('updateTOC()', () => 
  {
  var savedGulp;
  beforeEach(() => savedGulp = { src:gulp.src, dest:gulp.dest });
  afterEach (() => Object.assign(gulp, savedGulp));

  function updateTOC(input)
    {
    var file = new Vinyl(
      {
      cwd: '/',
      base: '/test/',
      path: '/test/file.js',
      contents: Buffer.from(input)
      });

    gulp.src  = () => 
      {
      var src = new stream.Readable({objectMode:true});
      src.push(file);
      src.push(null);
      return src;
      };
    gulp.dest = () => new stream.Writable({objectMode:true, write:(chunk, enc, cb) => cb()});

    return common.updateTOC();
    }

  it('Inserts a Table of Contents in a Markdown file.', (done) =>
    {
    var foundFile = false;
    var findTag   = false;

    updateTOC('# Title\n<!-- toc -->\n## Heading1\nText\n### Heading2\nText')
      .progress(file => 
        {
        foundFile = true;
        findTag  = /<!--[ \t]*tocstop[ \t]*-->/.test(file.contents.toString());
        })
      .then(() => 
        {
        assert.isTrue(foundFile, '<!-- toc ---> tag not found.');
        assert.isTrue(findTag,  '<!-- toc ---> tag found but TOC not created.');
        })
      .then(done)
      .catch(done)
    });

  it('Ignores a file without <!-- toc --> tag.', (done) =>
    {
    var foundFile = false;

    updateTOC('# Title\n## Heading1\nText\n### Heading2\nText')
      .progress(file => foundFile = true)
      .then(() => assert.isFalse(foundFile, 'File without <!-- toc --> tag was not ignored.'))
      .then(done)
      .catch(done)
    });
  });
