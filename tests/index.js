const common = require('../index');
const expect = require('chai').expect;
const path   = require('path');
const os     = require('os');

describe('Simple Functions (index)', () => 
  {
  it('Add and Remove PATH segments.', (done) =>
    {
    const path1 = 'path1/path3';
    const path2 = 'path2/path4'
    const orig  = process.env['PATH'];
    common.addToPath(path1, path2);
    expect(process.env['PATH']).to.have.string(path1.replace('/', path.sep));
    expect(process.env['PATH']).to.have.string(path2.replace('/', path.sep));
    common.removeFromPath(path2);
    expect(process.env['PATH']).to    .have.string(path1.replace('/', path.sep));
    expect(process.env['PATH']).to.not.have.string(path2.replace('/', path.sep));
    common.removeFromPath(path1);
    expect(process.env['PATH']).to.have.string(orig);

    common.addToPath([path1, path2]);
    expect(process.env['PATH']).to.have.string(path1.replace('/', path.sep));
    expect(process.env['PATH']).to.have.string(path2.replace('/', path.sep));
    common.removeFromPath(path2);
    expect(process.env['PATH']).to    .have.string(path1.replace('/', path.sep));
    expect(process.env['PATH']).to.not.have.string(path2.replace('/', path.sep));
    common.removeFromPath(path1);
    expect(process.env['PATH']).to.have.string(orig);
    done();
    });

  it('Normalize line endings.', (done) =>
    {
    const eol    = os.EOL;
    const input  = "CR:\r, LF:\n, CRCRLF:\r\n\n, CRCRLFLF:\r\r\n\n, CRLF:\r\n, LF:\n";
    const output = `CR:\r, LF:${eol}, CRCRLF:${eol}${eol}, CRCRLFLF:\r${eol}${eol}, CRLF:${eol}, LF:${eol}`;
    expect(common.normalizeEOL(input)).to.equal(output);
    done();
    });
  });
                                            