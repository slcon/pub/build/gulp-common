/*******************************************************************************
* $Id$
*******************************************************************************/
var glob = typeof window === 'undefined' ? global : window;

if (typeof glob.describe === 'undefined') {
    glob.describe = function() {};
    glob.it = function() {};
}

module.exports =
  {
  mock: (fn, mockFn) => 
    {
    mockFn.actual = fn;
    return mockFn;
    },

  unmock: (mockFn) => 
    {
    if (!mockFn.actual)
      throw "function is not mocked.";
    var fn = mockFn.actual;
    delete mockFn.actual;
    return fn;
    }
  };

