/*****************************************************************************************
* $Id$
/**
* Loads the functions in `funcpath` javascript files and returns a map keyed by the file 
* name. Javascript files must be function factories, injected with `$module`, and
* returning the function to add to the map.
*
* @name  makeFunctions
* @param  $module   Module function injected into each function factory.
* @param  dirname   `$module` file `__dirname`.
* @param  funcpath  Functions file path relative to `__dirname`, default=`funcs`.
* @returns          Map of function names to functions.
*****************************************************************************************/
(function() { 'use strict';

const fs   = require('fs');
const path = require('path');

module.exports = (common) => ($module, dirname, funcpath) =>
  {
  var fullpath = path.join(dirname, funcpath||'funcs');
  return (!fs.existsSync(fullpath) ? {} :
           fs.readdirSync(fullpath)
             .filter(n => n.endsWith('.js'))
             .map   (n => path.basename(n, '.js'))
             .reduce((m, t) => Object.assign(m, {[t]: require(path.join(fullpath, t))($module)}), {}));
  }
})();
