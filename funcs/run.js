/*****************************************************************************************
* $Id$
/**
* Runs `cmd` on the command line. Echos `cmd` and output to console as "stdout[\nstderr]"
* on success. If the command fails, the promise is rejected with fatal "err.message" or 
* `{error:err.message, stdout:stdout, stderr:stderr}` depending on `nonfatal` option.
*
* `options` will be passed to `child_process.exec()` but may also may include:
*   - {boolean} quiet    `true`=do not echo successful command output.
*   - {boolean} silent   `true`=do not echo command name or successful output.
*   - {boolean} nonfatal `true`=reject errors with error information.
*                        `false`=reject errors with fatal error message.
*
* @name    run
* @param   {string}  cmd   Command to execute.
* @param   {object}  opts  `child_process` + additional options (optional).
* @returns {Promise} Resolved with command output, rejected with fatal error info. 
*****************************************************************************************/
(function() { 'use strict';

const colors = require('ansi-colors');
const exec   = require('child_process').exec;
const Q      = require('q');

module.exports = (common) => ((cmd, opts) =>
  {
      opts     =   opts||{};
  var quiet    = !!opts.quiet;
  var silent   = !!opts.silent;
  var nonfatal = !!opts.nonfatal;

  if (!silent)
    common.options.logger.log(colors.cyan(cmd));

  return Q.Promise((resolve, reject) => 
    {
    exec(cmd, opts, (err, stdout, stderr) =>
      {
      stdout  = stdout.replace(/\s*(\r?\n)?$/, '');
      stderr  = stderr.replace(/\s*(\r?\n)?$/, '');

      if (err)
        {
        err.message = err.message.replace(/\s*(\r?\n)?$/, '');
        if (!nonfatal)
          reject(common.fatal(err.message));
        else
          reject({error:err.message, stdout:stdout, stderr:stderr});
        }
      else
        {
        var output = (stdout||"")+(stderr?`\n${stderr}`:"");
        if (!silent && !quiet && !!output)
          common.options.logger.log(colors.green(output));
        resolve(output);
        }
      });
    });
  });
})();
