/*****************************************************************************************
* $Id: funcs/addTasksFromMap.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
/**
* Adds a list of defined tasks from the module task map to the Gulp instance set with
* `useGulp()`. Task map is keyed by task name and each value is a path to the task 
* function javascript file or the task function, itself.
*
* @name   addTasksFromMap
* @param  {object}              map    Map of Gulp tasks from `loadTasks`.
* @param  {string[]|string...}  tasks  List of task names. undefined|null=add all.
*****************************************************************************************/
(function() { 'use strict';

const USAGE = 'usage: addTasksFromMap(taskMap [, taskName[]|[taskName...]])';

module.exports = (common) => function(map)
  {
  if (!map || typeof map !== 'object')
    throw USAGE;

  /*-------------------------------------------*/
  /* Task list may be in first argument array. */
  /*-------------------------------------------*/
  var args = Array.prototype.slice.call(arguments, 1);

  if (args.length)
    if (Array.isArray(args[0]))
      if (args.length > 1)
        throw USAGE;
      else
        args = args[0];

  /*-----------------------------------------------*/
  /* Task list is first array argument, all string */
  /* arguments, or all tasks if no names supplied. */
  /*-----------------------------------------------*/
  var tasks = (args.length ? Array.isArray(args[0]) ? args[0] : [ args[0] ] : Object.keys(map));

  /*-------------------------------------------*/
  /* Verify all task names are strings and all */
  /* values are either a function or a string. */
  /*-------------------------------------------*/
  if (tasks.some(e => typeof e !== 'string'))
    throw USAGE;

  /*--------------------------------------*/
  /* Verify tasks are in the task map and */
  /* point to a function or a file path.  */
  /*--------------------------------------*/
  var errors = tasks.map(t =>
    {
    if (!map[t])
      return `task '${t}' is not defined.`;
    if (['string', 'function'].indexOf(typeof map[t]) < 0)
      return `task '${t}' is not a file path or function.`;
    return null;
    }).filter(m => !!m);

  if (errors.length)
    throw errors.join('\n');

  /*-------------------------------------------------*/
  /* Build map of task names to task map value, load */
  /* functions from task files, and check function   */
  /* `.dependencies` property for dependent tasks.   */
  /*-------------------------------------------------*/
  tasks = tasks.map(t => ({ name:t, module:map[t] }));

  while(true)
    {
    /*---------------------------------*/
    /* Load task functions from files. */
    /*---------------------------------*/
    tasks.filter(t => typeof t.module !== 'function').forEach(t => t.module = require(t.module));

    /*----------------------------------------------------*/
    /* Build list of dependencies from task function      */
    /* `.dependencies` array property that are not        */
    /* already in the tasks list but are in the task map. */
    /*----------------------------------------------------*/
    var deps = tasks.filter( t     => Array.isArray(t.module.dependencies))
                    .map   ( t     => t.module.dependencies)
                    .reduce((a, d) => a.concat(d), [])
                    .filter( dt    => !!map[dt])
                    .filter( dt    => tasks.findIndex(t => t.name === dt) < 0);
    
    /*---------------------------------------------------*/
    /* If there are dependent tasks that must be loaded, */
    /* add to the tasks list and go around again.        */
    /*---------------------------------------------------*/
    if (deps.length > 0)
      tasks = tasks.concat(deps.map(dt => ({ name:dt, module:map[dt] })));
    else
      break;
    }

  /*--------------------*/
  /* Add tasks to gulp. */
  /*--------------------*/
  tasks.forEach(t => common.gulp.task(t.name, t.module.dependencies, t.module));
  }
})();
