/*****************************************************************************************
* $Id$
/**
* Reads `taskpath` for javascript task file names and returns map of task names to file 
* paths for use with `addTasks`.
*
* @name   loadTaskMap
* @param  dirname   `$module` file `__dirname`.
* @param  taskpath  Task file path relative to `dirname`, default=`tasks`.
* @returns          Map of task names to task file paths.
*****************************************************************************************/
(function() { 'use strict';

const fs   = require('fs');
const path = require('path');

module.exports = (common) => (dirname, taskpath) =>
  {
  var fullpath = path.join(dirname, taskpath||'tasks');
  return (!fs.existsSync(fullpath) ? {} :
           fs.readdirSync(fullpath)
             .filter(n => n.endsWith('.js'))
             .map   (n => path.basename(n, '.js'))
             .reduce((m, t) => Object.assign(m, {[t]: path.join(fullpath, t)}), {}));
  }
})();
