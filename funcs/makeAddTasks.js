/*****************************************************************************************
* $Id$
/**
* Returns a function returning a call to `addTasksFromMap` with `this.tasks`. Any module 
* that has tasks may use this to add the `addTasks` method to the module.
* 
* @name   makeAddTasks
* @return `addTasks` method.
*****************************************************************************************/
(function() { 'use strict';

module.exports = (common) => () => function() 
  { 
  common.addTasksFromMap.apply(common, [this.tasks].concat(Array.prototype.slice.call(arguments, 0))); 
  }
})();
