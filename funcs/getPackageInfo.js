/*****************************************************************************************
* $Id$
/**
* Returns `package.json` content. Throws fatal if `name` or `version` is missing.
*
* @name   getPackageInfo
* @return {object}  Package information.
*****************************************************************************************/
(function() { 'use strict';

const fs = require('fs');

module.exports = (common) => (() =>
  {
  const info = JSON.parse(fs.readFileSync("./package.json").toString('utf-8'));
  if (!!info.name && !!info.version)
    return info;
  throw common.fatal("Unable to parse 'package.json'");
  });
})();
