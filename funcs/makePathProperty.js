/*****************************************************************************************
* $Id: funcs/makePathProperty.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
/**
* Returns a Javascript property descriptor, for use with `Object.defineProperties()`, 
* describing accessors for a path segment property that will add the segment to the PATH
* environment variable when a new value is assigned. The descriptor defines two properties:
* 
*   - **`_${name}`**  Path segment value backing store.
*   - **`${name}`**   Accessor that removes the old and adds the new segment to PATH.  
* 
* @name  makePathProperty
* @param  {string}  name  Property name. 
* @return {object}        Property descriptors for use with `Object.defineProperties()`.
*****************************************************************************************/
(function() { 'use strict';

module.exports = (common) => (name) =>
  {
  const pname = `_${name}`;
  return 0,
    {
    [pname]: { writable: true },
    [name]:
      {
      enumerable: true,
      get: function()  { return this[pname]; }, 
      set: function(s) 
        {
        common.removeFromPath(this[pname]);
        common.addToPath     (this[pname] = s);
        }
      }
    };
  };
})();
