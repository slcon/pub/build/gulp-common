/*****************************************************************************************
* $Id: funcs/updateTOC.js 6ae811c 2022-03-29 06:01:25 -0700 sthames42 $
/**
* Finds all files matching `globs` that include a `<!-- toc -->` tag, inserts a Table of
* contents using `markdown-toc`, and updates the file. Matching files that do not have 
* the tag are not modified.
*
* @name    updateTOC
* @param   globs     Markdown file glob path(s).
* @param   options   `markdown-toc` options (optional).
* @returns {Promise} Resolved on success, rejected with fatal error info.
*                    Sends updated file object as progress messages.
*****************************************************************************************/
(function() { 'use strict';

const os      = require('os');
const Q       = require('q');
const toc     = require('markdown-toc');
const streams = require('event-stream');

module.exports = (common) => (globs, options) => 
  Q.Promise((resolve, reject, notify) =>
    common.gulp.src(globs)
    .pipe(streams.through(function(file) 
      {
      var input = file.contents.toString('utf8');
      if (/<!--[ \t]*toc[ \t]*-->/.test(input))
        {
        toc(input, options);
        var output    = toc.insert(input).replace(/\r?\n/g, os.EOL);
        file.contents = Buffer.from(output, 'utf8');
        this.push(file);
        notify(file);
        }
      }))
    .pipe(common.gulp.dest(file => file.base))
    .on('finish', resolve));
})();

