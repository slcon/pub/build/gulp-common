/*****************************************************************************************
* $Id$
/**
* Creates and returns a `namespace` module function to assign to `module.exports`. By 
* default, this function takes input of an object of module options and assigns it to 
* `this.options`. 
*
* Returned module function includes properties:
*
*   - **`options`**    Module options,
*   - **`tasks`**      Map of tasks created with `getTaskMap`.
*   - **`addTask`**    Function used to add tasks from `tasks` to gulp.
*   - **Function...**  Functions added to module with `makeFunctions`.
*
* @name  makeModule
* @param {string}   dirname   `__dirname` of calling module.
* @param {function} modfunc   Alternate package function, null=use default.
* @param {string}   funcpath  Functions file path relative to `__dirname`, default=`funcs`.
* @param {string}   taskpath  Task file path relative to `__dirname`, default=`tasks`.
* @return                     Module function with loaded methods and tasks.
*****************************************************************************************/
(function() { 'use strict';

module.exports = function(common) 
  {
  var makeModule = function(dirname, modfunc, funcpath, taskpath)
    {
    const $module = modfunc || makeModule.defaultFunction;

    return Object.assign($module, common.makeFunctions($module, dirname, funcpath),
      {
      addTasks: common.makeAddTasks(),
      tasks:    common.getTaskMap(dirname, taskpath),
      useGulp:  makeModule.useGulp
      });
    };

  /*---------------------------------------------------*/
  /* Add `defaultFunction` and `useGulp` so the        */
  /* module can be built "the hard way", if necessary. */
  /*---------------------------------------------------*/
  Object.defineProperty(makeModule, "defaultFunction", { get: () => module.exports.defaultFunction });

  /*---------------------------------------------------------*/
  /* useGulp - Set the `gulp` instance in the common module. */
  /*---------------------------------------------------------*/
  makeModule.useGulp = function(gulp) 
    { 
    common.useGulp(gulp);
    return this;
    }

  return makeModule;
  }

/*------------------------------------------------*/
/* defaultFunction - Returns a new module         */
/* function. Can't return a static one or modules */
/* that use common will get the same function.    */
/*------------------------------------------------*/
Object.defineProperty(module.exports, "defaultFunction",
  {
  get: function()
    {
    const $module = function(options)
      {
      Object.assign($module.options, options);
      return $module;
      };
    return $module;
    }
  });
})();
