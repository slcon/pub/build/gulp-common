/*****************************************************************************************
* $Id$
/**
* Runs a list of tasks synchronously. `runSequence` does not return a promise but calls a
* callback method when all tasks are finished. This method returns a promise and adds a
* callback to the list of tasks which will resolve when the callback is called.
*
* @name    runTasks
* @param   tasks...  List of tasks to pass to `runSequence`.
* @returns {Promise} Resolved on success, rejected with fatal error info. 
*****************************************************************************************/
(function() { 'use strict';

const Q           = require('q');
const runSequence = require('run-sequence');

module.exports = (common) => function()
  {
  return Q.Promise((resolve, reject) => 
    {
    var cb   = (error) => (error ? reject(common.fatal(error)) : resolve());
    var args = Array.prototype.slice.call(arguments, 0).concat([cb]);
    runSequence.use(common.gulp).apply(null, args);
    });
  };
})();
